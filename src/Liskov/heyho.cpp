#include <iostream>

class Weird
{
public:
    void sayHey()
        {
            std::cout << "Heeeeeeeeeeey ";
        }
};

class thisIsWeirdToo : public Weird
{
public:
    void sayHo()
    {
        std::cout << "Hoooooooooooo";
    }
};

int main()
{
    Weird* a = new Weird();
    Weird* a2 = new thisIsWeirdToo();

    a ->sayHey();
    a2 ->sayHo();
}
