class Weird
{
public void sayHey()
        {
            System.out.print("Heeeeeeeeeeey ");
        }
};

class thisIsWeirdToo extends Weird
{
public void sayHo()
    {
        System.out.print("Hoooooooooooo");
    }
};

class thisIsNotWeird
{
public static void main(String[] args)
{
    Weird a = new Weird();
    Weird a2 = new thisIsWeirdToo();

    a.sayHey();
    a2.sayHo();
}
}
