#include <iostream>

int main()
{
    char choice;
    std::cout <<"Please choose between a or b! ";
    std::cin >>choice;
    if (choice=='a')
        std::cout <<"You chose a! \n";
    else if (choice=='b')
        std::cout <<"You chose b! \n";
    else
        std::cout <<"You didn't choose. :( \n";
}
